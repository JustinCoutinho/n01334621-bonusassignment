﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Question 2.aspx.cs" Inherits="BonusAssignment.Question_2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <label>Prime Number Validator</label>
            <asp:TextBox runat="server" ID="modinput"></asp:TextBox>
            <br />
            <asp:CompareValidator runat="server" ID="numberVal" ControlToValidate="modinput" Type="integer" Operator="DataTypeCheck" ErrorMessage="Please input a number" />

            <asp:Button ID="submit" Text="Submit" OnClick="Button_Click" runat="server"/>

        <div id="output" runat="server">
        </div>
        </div>
    </form>
</body>
</html>
