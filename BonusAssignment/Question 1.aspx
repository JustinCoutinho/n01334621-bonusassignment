﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Question 1.aspx.cs" Inherits="BonusAssignment.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <label>X</label>
        <asp:Textbox runat="server" ID="xvalue"></asp:Textbox>
        <asp:RequiredFieldValidator runat="server" id="reqfieldx" controltovalidate="xvalue" errormessage="Please enter a value" />
        <asp:CompareValidator runat="server" ID="numberVal" ControlToValidate="xvalue" Type="integer" Operator="DataTypeCheck" ErrorMessage="Please input a number" />

            <br />
        <label>Y</label>
        <asp:Textbox runat="server" ID="yvalue"></asp:Textbox>
        <asp:RequiredFieldValidator runat="server" id="reqfieldy" controltovalidate="yvalue" errormessage="Please enter a value" />
        <asp:CompareValidator runat="server" ID="CompareValidator1" ControlToValidate="yvalue" Type="integer" Operator="DataTypeCheck" ErrorMessage="Please input a number" />

            <br />
        <asp:Button ID="submit" Text="Submit" OnClick="Button_Click" runat="server"/>

        <div id="output" runat="server">
        </div>
        </div>
    </form>
</body>
</html>
