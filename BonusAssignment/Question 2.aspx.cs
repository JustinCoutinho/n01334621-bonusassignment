﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BonusAssignment
{
    public partial class Question_2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
            
        protected void Button_Click(object sender, EventArgs e)
        {
            int num = int.Parse(modinput.Text);
        /*This method for the variable 'limit' was found in 
        https://stackoverflow.com/questions/15743192/check-if-number-is-prime-number */
            var limit = Math.Ceiling(Math.Sqrt(num));

            if (num == 1)
            {
               output.InnerHtml = num + " is not a prime number";
            }
            else if (num == 2)
            {
                output.InnerHtml = num + " is a prime number";
            }
            else

            for (int i = 2; i <= limit; ++i)
            {
                if (num % i == 0)
                {
                    output.InnerHtml = num + " is not prime number";
                }
                else
                {
                    output.InnerHtml = num + " is a prime number";
                }
            }

        }






    }
    
}