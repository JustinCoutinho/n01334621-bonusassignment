﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Question 3.aspx.cs" Inherits="BonusAssignment.Question_3" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head> 
<body>
    <form id="form1" runat="server">
        <div>
        <label>Palindrome Validator</label>
        <asp:TextBox runat="server" ID="inputword"></asp:TextBox>
        <asp:RegularExpressionValidator runat="server" ID="testvalidation" ControlToValidate="inputword" ValidationExpression="^[a-zA-Z ]+$" ErrorMessage="Please enter a word." />
        <br />
        <asp:Button ID="submitbutton" Text="Submit" OnClick="Button_Click" runat="server"/>
        <div id="output" runat="server">
        </div>
        </div>
    </form>
</body>
</html>
