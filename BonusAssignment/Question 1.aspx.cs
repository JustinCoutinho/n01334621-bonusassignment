﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BonusAssignment
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button_Click(object sender, EventArgs e)
        {
            int x = int.Parse(xvalue.Text);
            int y = int.Parse(yvalue.Text);

            if (x > 0 && y > 0)
            {
                output.InnerHtml = "Quadrant 1";
            } else if (x < 0 && y > 0)
            {
                output.InnerHtml = "Quadrant 2";
            } else if (x < 0 && y < 0)
            {
                output.InnerHtml = "Quadrant 3";
            } else if (x > 0 && y < 0)
            {
                output.InnerHtml = "Quadrant 4";
            } else if (x == 0 || y == 0)
            {
                output.InnerHtml = "This input does not fall in any of the quadrants";
            }


            

        }
    }
}