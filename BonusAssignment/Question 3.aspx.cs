﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BonusAssignment
{
    public partial class Question_3 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void Button_Click(object sender, EventArgs e)
        {


            string input = inputword.Text.ToLower().Replace(" ",string.Empty);
            string reverse = string.Empty;
            //Loop referenced from https://stackoverflow.com/questions/228038/best-way-to-reverse-a-string
            for (int i = input.Length - 1; i > -1; i--)
            {
                reverse += input[i]; 
            }
            output.InnerHtml = reverse;
            if (reverse == input)
            {
                output.InnerHtml = input + " is a palindrome";
            }
            else {
                output.InnerHtml = input + " is not a palindrome";
            }
        }

         
    }
}